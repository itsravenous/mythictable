<!-- Title suggestion: [Onboarding] Newcomer's name -->

# Welcome to Mythic Table

We hope you're as excited as we are for being a part in this mythic project.

Please got through the items below and check them off as they are completed:

## **1. Learn Mythic Table**

It is recommended that each of these are bookmarked

### Required

- [ ] [*Main Site*](https://www.mythictable.com/)
- [ ] [*Non-profit Announcement*](https://www.mythictable.com/org/now-non-profit)
- [ ] [*Mission Statement*](https://www.mythictable.com/mission)
- [ ] [*Features*](https://www.mythictable.com/features)
- [ ] [*Wiki*](https://gitlab.com/mythicteam/mythictable/-/wikis/home)
- [ ] [*Gitlab*](https://gitlab.com/mythicteam/mythictable)

### Optional

- [ ] [*Mythic Table Dev Stories*](https://www.mythictable.com/team/mythicstories/index/)

Join us on our social media platforms:

- [ ] ![alt text](https://i.imgur.com/TtIyDnO.png "Youtube") - [*Youtube*](https://www.youtube.com/mythictable)
- [ ] ![alt text](https://i.imgur.com/RBSCNgo.png "Facebook") - [*Facebook*](https://www.facebook.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/M9RR4Hf.png "Instagram") - [*Instagram*](https://www.instagram.com/mythic_table/)
- [ ] ![alt text](https://i.imgur.com/hgpY9gu.png "Reddit") - [*Reddit*](https://www.reddit.com/r/mythictable/)
- [ ] ![alt text](https://i.imgur.com/rEAMp9o.png "Twitter") - [*Twitter*](https://twitter.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/kX34PqK.png "Patreon") - [*Patreon*](https://www.patreon.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/m9k88St.png "Tumblr") - [*Tumblr*](https://mythictable.tumblr.com/)
- [ ] ![alt text](https://i.imgur.com/EbNeByg.png "Product Hunt") - [*Product Hunt*](https://www.producthunt.com/posts/mythic-table/)


## **2. Slack**

We use Slack for almost all of our communication. Being active in there is critical to being and effective team member

- [ ] Join the **#random** channel and tell us about yourself
- [ ] Reply to or start a **Thread**
- [ ] Send Marc a private message
- [ ] Customize your notifications

These are discipline channels. You should join at least one of these:

- [ ] Programmer - Join the **#dev** channel
- [ ] Operations or DevOps - Join the **#devops** channel
- [ ] Artist - Join the **#art** channel
- [ ] Designer - Join the **#design** channel
- [ ] Community manager - Join the **#community** channel

### Get Social!

- [ ] **Join #coffee-break**! We use this for our donut channel social meetups.  Get to know the rest of the team!

Other useful slack channels:
- [ ] **#daily**
- [ ] **#learn**

### Optional

- [ ] Get Slack on your phone

## **3. Process**

Mythic Table doesn't have a lot of process, but what we have we've adopted for very good reasons.  Learn about our process here.

- [ ] [Introduction](https://gitlab.com/mythicteam/mythictable/-/wikis/Working-on-Mythic-Table)
- [ ] [Guidelines](https://gitlab.com/mythicteam/mythictable/-/wikis/workflow-guidelines)
- [ ] [Milestones](https://gitlab.com/mythicteam/mythictable/-/milestones)
- [ ] [Kanban Board](https://gitlab.com/mythicteam/mythictable/-/boards)


## **4. Git**

- [ ] [*Try a tutorial*](https://try.github.io/)

0r

- [ ] Already a pro

Git is slowly becoming a software industry standard. We have some rather specific rules around how we use it at Mythic Table

- [ ] [How to Branch and Merge on Mythic Table](https://gitlab.com/mythicteam/mythictable/-/wikis/how-to/branching)


## **5. Your First Commit**

We work really hard to make sure that Local Development is as easy as possible. However
there are a lot of little steps that should be followed carefully.

> *If you need any help or have doubts don't be afraid to ask in the #dev channel.* 

- [ ] Clone the Git repo `git clone git@gitlab.com:mythicteam/mythictable.git`
- [ ] Create a new branch `git checkout -b issue/##-your-name-onboarding`
- [ ] Find and edit CONTRIBUTORS.md - Add your name ;)
- [ ] Stage your changes `git add CONTRIBUTORS.md`
- [ ] Commit your changes `git commit -m "Issue #??: Adding myself to CONTRIBUTORS.md"`
- [ ] Push your changes `git push -u origin issue/##-your-name-onboarding`

Next you'll create a merge request:

 ![alt text](https://i.imgur.com/xL3P4D5.png) 

- [ ] Visit https://gitlab.com/mythicteam/mythictable/-/merge_requests
- [ ] Create a new merge request. If you're lucky there will be a quick create option for your new branch when you arrive.
- [ ] Be sure to use a template for your Merge Request
- [ ] Ask someone to review it on slack
- [ ] Squash, Merge and Delete your branch. You're done!

## **6. Local Dev**

We work really hard to make sure that Local Development is as easy as possible. However
there are a lot of little steps that should be followed carefully. Since the instructions
are apt to change without notice to reflect new processes and systems, steps for this can
be found in the code.

- [ ] [README.md](https://gitlab.com/mythicteam/mythictable/-/blob/main/README.md)
- [ ] Launch Mythic Table Service
- [ ] Launch Vue Frontend Development server
- [ ] Launch Authentication Service
- [ ] Try Mythic Table


## **7. Tell the world!**

- [ ] **Join us on Linkedin** https://www.linkedin.com/company/mythic-table/

/label ~"type::onboarding"
