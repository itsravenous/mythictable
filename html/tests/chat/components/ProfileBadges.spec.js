import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import ProfileBadges from '@/chat/components/ProfileBadges.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('ProfileBadges', () => {
    const getters = {
        getGroups: () => id => {
            if (id == '') {
                return [];
            }
            return id.split(';');
        },
    };

    const store = new Vuex.Store({
        modules: {
            profile: {
                namespaced: true,
                getters,
            },
        },
    });

    function buildWrapper(id = '') {
        return shallowMount(ProfileBadges, {
            propsData: {
                userId: id,
            },
            localVue,
            store,
        });
    }

    it('No groups should show no badges', () => {
        const wrapper = buildWrapper();
        expect(wrapper.findAll('.icon').length).toBe(0);
    });

    it('Unknown group should show no badges', () => {
        const wrapper = buildWrapper('garbage group');
        expect(wrapper.findAll('.icon').length).toBe(0);
    });

    describe('Kickstarter Group', () => {
        it('Kickstarter group should show 1 badge', () => {
            const wrapper = buildWrapper('Kickstarter Backer');
            expect(wrapper.findAll('.icon').length).toBe(1);
        });

        it('Kickstarter group should have class .Kickstarter_Backer', () => {
            const wrapper = buildWrapper('Kickstarter Backer');
            expect(wrapper.findAll('.Kickstarter_Backer').length).toBe(1);
        });

        it('Kickstarter group should have appropriate font-awesome attributes', () => {
            const wrapper = buildWrapper('Kickstarter Backer');
            const element = wrapper.findAll('.Kickstarter_Backer').at(0);
            expect(element.attributes().icon).toBe('fab,kickstarter-k');
        });
    });
});
